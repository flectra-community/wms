# Flectra Community / wms

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[shopfloor](shopfloor/) | 2.0.1.0.1| manage warehouse operations with barcode scanners
[shopfloor_mobile_base](shopfloor_mobile_base/) | 2.0.1.3.0| Mobile frontend for WMS Shopfloor app
[shopfloor_checkout_sync](shopfloor_checkout_sync/) | 2.0.1.0.0| Glue module
[shopfloor_mobile_base_auth_api_key](shopfloor_mobile_base_auth_api_key/) | 2.0.1.0.0| Provides authentication via API key to Shopfloor base mobile app
[stock_picking_type_shipping_policy_group_by](stock_picking_type_shipping_policy_group_by/) | 2.0.1.0.0| Glue module for Picking Type Shipping Policy and Group Transfers by Partner and Carrier
[shopfloor_delivery_shipment_mobile](shopfloor_delivery_shipment_mobile/) | 2.0.1.0.0| Frontend for delivery shipment scenario for shopfloor
[stock_storage_type_putaway_abc](stock_storage_type_putaway_abc/) | 2.0.1.0.0| Advanced storage strategy ABC for WMS
[shopfloor_base](shopfloor_base/) | 2.0.1.0.2| Core module for creating mobile apps
[sale_stock_available_to_promise_release](sale_stock_available_to_promise_release/) | 2.0.1.0.0| Integration between Sales and Available to Promise Release
[stock_move_source_relocate_dynamic_routing](stock_move_source_relocate_dynamic_routing/) | 2.0.1.0.0| Glue module
[stock_move_source_relocate](stock_move_source_relocate/) | 2.0.1.0.0| Change source location of unavailable moves
[shopfloor_workstation](shopfloor_workstation/) | 2.0.1.1.0| Manage warehouse workstation with barcode scanners
[shopfloor_example](shopfloor_example/) | 2.0.1.0.0| Show how to customize the Shopfloor app frontend.
[stock_picking_completion_info](stock_picking_completion_info/) | 2.0.1.0.0| Display on current document completion information according to next operations
[stock_storage_type_buffer](stock_storage_type_buffer/) | 2.0.1.3.0| Exclude storage locations from put-away if their buffer is full
[stock_picking_type_shipping_policy](stock_picking_type_shipping_policy/) | 2.0.1.0.0| Define different shipping policies according to picking type
[shopfloor_mobile_base_auth_user](shopfloor_mobile_base_auth_user/) | 2.0.1.0.0| Provides authentication via standard user login
[shopfloor_workstation_mobile](shopfloor_workstation_mobile/) | 2.0.1.0.0| Shopfloor mobile app integration for workstation
[shopfloor_mobile](shopfloor_mobile/) | 2.0.1.1.0| Mobile frontend for WMS Shopfloor app
[stock_reception_screen_qty_by_packaging](stock_reception_screen_qty_by_packaging/) | 2.0.1.0.0| Glue module for `stock_product_qty_by_packaging` and `stock_vertical_lift`.
[stock_available_to_promise_release](stock_available_to_promise_release/) | 2.0.1.0.1| Release Operations based on available to promise
[shopfloor_delivery_shipment](shopfloor_delivery_shipment/) | 2.0.1.0.0| Manage delivery process with shipment advices
[stock_reception_screen_measuring_device](stock_reception_screen_measuring_device/) | 2.0.1.0.0| Allow to use a measuring device from a reception screen.for packaging measurement
[stock_picking_consolidation_priority](stock_picking_consolidation_priority/) | 2.0.1.0.1| Raise priority of all transfers for a chain when started
[sale_stock_available_to_promise_release_dropshipping](sale_stock_available_to_promise_release_dropshipping/) | 2.0.1.0.0| Glue module between sale_stock_available_to_promise_release and stock_dropshipping
[stock_dynamic_routing](stock_dynamic_routing/) | 2.0.1.0.0| Dynamic routing of stock moves
[stock_reception_screen](stock_reception_screen/) | 2.0.1.0.0| Dedicated screen to receive/scan goods.
[shopfloor_packing_info](shopfloor_packing_info/) | 2.0.1.0.0| Allows to predefine packing information messages per partner.
[stock_dynamic_routing_reserve_rule](stock_dynamic_routing_reserve_rule/) | 2.0.1.0.0| Glue module between dynamic routing and reservation rules
[delivery_carrier_warehouse](delivery_carrier_warehouse/) | 2.0.1.2.0| Get delivery method used in sales orders from warehouse
[shopfloor_batch_automatic_creation](shopfloor_batch_automatic_creation/) | 2.0.1.0.0| Create batch transfers for Cluster Picking
[delivery_carrier_preference](delivery_carrier_preference/) | 2.0.1.0.1| Advanced selection of preferred shipping methods
[shopfloor_rest_log](shopfloor_rest_log/) | 2.0.1.0.0| Integrate rest_log into Shopfloor app
[delivery_preference_glue_stock_picking_group](delivery_preference_glue_stock_picking_group/) | 2.0.1.0.0| Fix Delivery preferences module on grouping picking
[stock_storage_type](stock_storage_type/) | 2.0.1.9.0| Manage packages and locations storage types
[stock_dynamic_routing_checkout_sync](stock_dynamic_routing_checkout_sync/) | 2.0.1.0.0| Glue module for tests when dynamic routing and checkout sync are used 
[stock_checkout_sync](stock_checkout_sync/) | 2.0.1.0.0| Sync location for Checkout operations
[stock_available_to_promise_release_dynamic_routing](stock_available_to_promise_release_dynamic_routing/) | 2.0.1.0.0| Glue between moves release and dynamic routing


