# Copyright 2019 Camptocamp (https://www.camptocamp.com)
{
    "name": "Stock Dynamic Routing - Checkout Sync",
    "summary": "Glue module for tests when dynamic routing and"
    " checkout sync are used ",
    "author": "Camptocamp, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/wms",
    "category": "Warehouse Management",
    "version": "2.0.1.0.0",
    "license": "AGPL-3",
    "depends": ["stock_dynamic_routing", "stock_checkout_sync"],
    "demo": [],
    "data": [],
    "auto_install": True,
    "installable": True,
    "development_status": "Alpha",
}
