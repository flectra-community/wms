# Copyright 2021 Camptocamp (https://www.camptocamp.com)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    "name": "Sale Stock Available to Promise Release Cutoff",
    "summary": "Cutoff management with respect to stock availability",
    "version": "2.0.1.1.1",
    "author": "Camptocamp,Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/wms",
    "category": "Stock Management",
    "depends": [
        "sale_stock_available_to_promise_release",
        "sale_delivery_date",
    ],
    "installable": True,
    "auto_install": True,
    "license": "AGPL-3",
    "application": False,
    "development_status": "Alpha",
}
